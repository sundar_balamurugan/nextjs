import Link from 'next/link';
import styles from './layout.module.css';

export default function Page2() {
    return (
        <div className={styles.container}>
            <h1 >Second page</h1>
            <Link href="/">
                <a>Back to home</a>
            </Link>
        </div>
    )
}