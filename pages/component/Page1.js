import Link from 'next/link'
import Page2 from './Page2'

export default function Page1() {
    return (
        <div>
            <h1>First page</h1>
            <h2>
                <Link href="/">
                    <a>Back to home</a>

                </Link>
            </h2>
            <Page2 />
        </div>
    )
}